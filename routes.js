const express = require("express")
const { createUser, loginUser, profile, findHeightByName } = require("./controllers/controllersUser")
const { createWeight, findAllWeightbyName } = require("./controllers/controllersWeight")
const { findAllProduct } = require("./controllers/controllersProducts")
const checkUpUsername =  require("./middleware/VerifyPseudo")
const verifyTokens = require("./middleware/authjwt")
const  router = express.Router()

router.post("/",   createUser)
router.get("/product", findAllProduct)
router.post("/login", loginUser)
router.get("/profile", verifyTokens, profile)


module.exports = router
