

module.exports = (
  sequelize, DataTypes
) => {
  return sequelize.define("user", {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement : true
    },
    name : {
      type : DataTypes.STRING,
      allowNull : false,
    },
  
    address: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    city: {
      type: DataTypes.STRING,
      allowNull: false
    },
    gender : {
      type: DataTypes.STRING,
      allowNull: false
    },
    email : {
      type: DataTypes.STRING,
      allowNull: false
    },
    phone: {
      type: DataTypes.STRING,
      allowNull: false
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false
    }


  },{
    timestamps: false
  });
};


