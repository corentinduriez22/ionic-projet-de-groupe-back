module.exports = (
    sequelize, DataTypes
  ) => {
    return sequelize.define("product", {
        id :{
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement : true
        },
        name:{
            type : DataTypes.STRING,
            allowNull : false,
        },
        imageList:{
            type : DataTypes.STRING,
            allowNull : false,
        },
        prixactuel: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        pricePassed:{
            type: DataTypes.INTEGER,
            allowNull: false
        },
        note:{
            type: DataTypes.INTEGER,
            allowNull: false
        },
        commentaire: {
            type : DataTypes.STRING,
            allowNull : false,
        },
        vue: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        description: {
            type : DataTypes.STRING,
            allowNull : false,
        },
        category: {
            type : DataTypes.STRING,
            allowNull : false,
        },
        listColor : {
            type : DataTypes.STRING,
            allowNull : false,
        },
        listWeight : {
            type : DataTypes.STRING,
            allowNull : false,
        }, 
        
           
        

    }, {
        timestamps: false
    })

    }

  